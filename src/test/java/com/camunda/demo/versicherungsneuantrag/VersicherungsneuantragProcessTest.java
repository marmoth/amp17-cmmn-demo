package com.camunda.demo.versicherungsneuantrag;

import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareAssertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.complete;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.execute;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.job;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.taskService;
import static org.camunda.bpm.engine.test.assertions.cmmn.CmmnAwareTests.withVariables;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.Variables.SerializationDataFormats;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.camunda.demo.versicherungsneuantrag.adapter.SendEmailService;
import com.camunda.demo.versicherungsneuantrag.model.Neuantrag;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = { "com.camunda.demo.versicherungsneuantrag.adapter.*" })
public class VersicherungsneuantragProcessTest {

  @Rule
  public ProcessEngineRule rule = new ProcessEngineRule();

  static {
    // enable more detailed logging
    // LogUtil.readJavaUtilLoggingConfigFromClasspath(); // process engine
    // LogFactory.useJdkLogging(); // MyBatis
  }

  @Before
  public void setup() {
    init(rule.getProcessEngine());
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Just tests if the process definition is deployable.
   */
  @Test
  @Deployment(resources = { "Versicherungsneuantrag.bpmn", "Dokumentenanforderung.bpmn", "Risikopruefung.dmn" })
  public void testParsingAndDeployment() {
    // nothing is done here, as we just want to check for exceptions during
    // deployment
  }

  @Mock
  private SendEmailService sendEmailService;

  @Test
  @Deployment(resources = { "Versicherungsneuantrag.bpmn", "Risikopruefung.dmn" })
  public void testDunkelverarbeitungPoliciert() throws Exception {
    PowerMockito.whenNew(SendEmailService.class).withAnyArguments().thenReturn(sendEmailService);

    Neuantrag neuantrag = DemoData.createNeuantrag(40, "VW", "Golf V");
    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    ProcessInstance processInstance = runtimeService().startProcessInstanceByKey(ProcessConstants.PROCESS_KEY_versicherungsneuantrag, variables);

    assertThat(processInstance).job();
    execute(job()); // start event
    execute(job()); // send task

    // Dunkelverarbeitung!
    assertThat(processInstance) //
        .isEnded() //
        .hasPassed("BusinessRuleTaskAntragAutomatischPruefen", "ServiceTaskPoliceAusstellen", "SendTaskPoliceZusenden", "EndEventAntragPoliciert");

    // verify that mail service was called correctly
    verify(sendEmailService, times(1)).sendEmail(eq(neuantrag.getAntragssteller().getEmail()), anyString(), anyString());
  }

  @Test
  @Deployment(resources = { "Versicherungsneuantrag.bpmn", "Risikopruefung.dmn" })
  public void testDunkelverarbeitungAbgelehnt() throws Exception {
    PowerMockito.whenNew(SendEmailService.class).withAnyArguments().thenReturn(sendEmailService);

    Neuantrag neuantrag = DemoData.createNeuantrag(20, "Porsche", "911");
    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    ProcessInstance processInstance = runtimeService().startProcessInstanceByKey(ProcessConstants.PROCESS_KEY_versicherungsneuantrag, variables);

    assertThat(processInstance).job();
    execute(job()); // start event
    execute(job()); // send task

    assertThat(processInstance) //
        .isEnded() //
        .hasPassed("BusinessRuleTaskAntragAutomatischPruefen", "SendTaskAblehnungZusenden", "EndEventAntragAbgelehnt");

    // verify that mail service was called correctly
    verify(sendEmailService, times(1)).sendEmail(eq(neuantrag.getAntragssteller().getEmail()), anyString(), anyString());
  }

  @Test
  @Deployment(resources = { "Versicherungsneuantrag.bpmn", "Risikopruefung.dmn", "Dokumentenanforderung.bpmn", "Neuantragspruefung.cmmn" })
  public void testHellManuelleVerarbeitung() throws Exception {
    PowerMockito.whenNew(SendEmailService.class).withAnyArguments().thenReturn(sendEmailService);

    Neuantrag neuantrag = DemoData.createNeuantrag(40, "BMW", "X3"); // I
    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    ProcessInstance processInstance = runtimeService().startProcessInstanceByKey(ProcessConstants.PROCESS_KEY_versicherungsneuantrag, variables);

    assertThat(processInstance).job();
    execute(job());

    Task task = taskService().createTaskQuery().singleResult();
    // assertEquals()
    // assertThat(processInstance).task()
    // .hasDefinitionKey("userTaskAntragEntscheiden")
    // .hasCandidateGroup("sachbearbeiter");

    complete(task, withVariables("approved", Boolean.TRUE));

    assertThat(processInstance).job();
    execute(job());

    assertThat(processInstance) //
        .isEnded() //
        .hasPassed("ServiceTaskPoliceAusstellen", "SendTaskPoliceZusenden");

    // verify that mail service was called correctly
    verify(sendEmailService, times(1)).sendEmail(eq(neuantrag.getAntragssteller().getEmail()), anyString(), anyString());
  }

}
