package com.camunda.demo.versicherungsneuantrag;

import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.init;
import static org.camunda.bpm.engine.test.assertions.bpmn.AbstractAssertions.processEngine;
import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.taskQuery;
import static org.camunda.bpm.engine.test.assertions.cmmn.CmmnAwareAssertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.cmmn.CmmnAwareTests.caseExecution;
import static org.camunda.bpm.engine.test.assertions.cmmn.CmmnAwareTests.complete;

import java.util.List;

import org.camunda.bpm.engine.CaseService;
import org.camunda.bpm.engine.history.HistoricCaseActivityInstance;
import org.camunda.bpm.engine.history.HistoricCaseActivityInstanceQuery;
import org.camunda.bpm.engine.runtime.CaseExecution;
import org.camunda.bpm.engine.runtime.CaseInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.Variables.SerializationDataFormats;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.camunda.demo.versicherungsneuantrag.model.Neuantrag;

public class NeuantragspruefungCaseTest {

  @Rule
  public ProcessEngineRule rule = new ProcessEngineRule();

  @Before
  public void setup() {
    init(rule.getProcessEngine());
  }

  /**
   * Just tests if the case definition is deployable
   */

  @Test
  @Deployment(resources = "Neuantragspruefung.cmmn")
  public void testParsingAndDeployment() {

  }

  @Test
  @Deployment(resources = "Neuantragspruefung.cmmn")
  public void testCaseNeuantragspruefungAngenommenOhneFreigabe() {
    Neuantrag neuantrag = DemoData.createNeuantrag(20, "Porsche", "911");
    neuantrag.setPreisindikationInCent(30000);

    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    CaseInstance caseInstance = processEngine().getCaseService().createCaseInstanceByKey( //
        ProcessConstants.CASE_KEY_Neuantragspruefung, variables);

    assertThat(caseInstance).stage("Stage_ApplicationDecision").isActive();
    assertThat(caseInstance).stage("Stage_ApplicationDecision").humanTask("HumanTask_DecideOnApplication").isActive();

    processEngine().getCaseService().setVariable(caseInstance.getId(), "approved", true);
    complete(caseExecution("HumanTask_DecideOnApplication", caseInstance), Variables.createVariables().putValue(//
        ProcessConstants.VAR_NAME_approved, Boolean.TRUE));

    // printCaseStatusAndTasklist();
    assertThat(caseInstance).isCompleted();
  }

  @Test
  @Deployment(resources = "Neuantragspruefung.cmmn")
  public void testCaseNeuantragspruefungAngenommenMitFreigabe() {
    Neuantrag neuantrag = DemoData.createNeuantrag(20, "Porsche", "911");
    neuantrag.setPreisindikationInCent(30001);

    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    CaseInstance caseInstance = processEngine().getCaseService().createCaseInstanceByKey( //
        ProcessConstants.CASE_KEY_Neuantragspruefung, variables);

    assertThat(caseInstance).stage("Stage_ApplicationDecision").isActive();
    assertThat(caseInstance).stage("Stage_ApplicationDecision").humanTask("HumanTask_DecideOnApplication").isActive();

    processEngine().getCaseService().setVariable(caseInstance.getId(), "approved", true);
    complete(caseExecution("HumanTask_DecideOnApplication", caseInstance), Variables.createVariables().putValue(//
        ProcessConstants.VAR_NAME_approved, Boolean.TRUE));
    complete(caseExecution("HumanTask_ApproveDecision", caseInstance));

    // printCaseStatusAndTasklist();
    assertThat(caseInstance).isCompleted();
  }

  @Test
  @Deployment(resources = "Neuantragspruefung.cmmn")
  public void testCaseNeuantragspruefungAbgelehntOhneFreigabe() {
    Neuantrag neuantrag = DemoData.createNeuantrag(20, "Porsche", "911");
    neuantrag.setPreisindikationInCent(30000);

    System.out.println(neuantrag.getPreisindikationInCent());
    
    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    CaseInstance caseInstance = processEngine().getCaseService().createCaseInstanceByKey( //
        ProcessConstants.CASE_KEY_Neuantragspruefung, variables);

    assertThat(caseInstance) //
        .stage("Stage_ApplicationDecision") //
        .isActive();
    assertThat(caseInstance) //
        .stage("Stage_ApplicationDecision") //
        .humanTask("HumanTask_DecideOnApplication") //
        .isActive();

    complete(caseExecution("HumanTask_DecideOnApplication", caseInstance), Variables.createVariables().putValue(//
        ProcessConstants.VAR_NAME_approved, Boolean.FALSE));

    // printCaseStatusAndTasklist();
    assertThat(caseInstance).isCompleted();
  }

  @SuppressWarnings("unused")
  private void printCaseStatusAndTasklist() {
    System.out.println("Case Status:");
    CaseService caseService = processEngine().getCaseService();
    List<CaseExecution> caseExecutions = caseService.createCaseExecutionQuery().list();
    for (CaseExecution caseExecution : caseExecutions) {
      if (caseExecution.isActive()) {
        System.out.println("  active:  " + caseExecution.getActivityName() + " (" + caseExecution.getActivityType() + ")");
      } else if (caseExecution.isEnabled()) {
        System.out.println("  enabled: " + caseExecution.getActivityName() + " (" + caseExecution.getActivityType() + ")");
      } else if (caseExecution.isTerminated()) {
        System.out.println("  terminated: " + caseExecution.getActivityName() + " (" + caseExecution.getActivityType() + ")");
      }
    }

    HistoricCaseActivityInstanceQuery historicCaseActivityInstanceQuery = processEngine().getHistoryService().createHistoricCaseActivityInstanceQuery()
        .completed();

    System.out.println("Case History (" + historicCaseActivityInstanceQuery.count() + "):");
    List<HistoricCaseActivityInstance> completedCaseActivities = historicCaseActivityInstanceQuery.list();
    for (HistoricCaseActivityInstance completedCaseActivity : completedCaseActivities) {
      System.out.println("  completed:  " + completedCaseActivity.getCaseActivityName() + " (" + completedCaseActivity.getCaseActivityType() + ")");
    }

    System.out.println("Task List (" + taskQuery().count() + "):");
    List<Task> tasklist = taskQuery().list();
    for (Task task : tasklist) {
      System.out.println("  " + (task.getAssignee() == null ? "unassigned" : task.getAssignee()) + ": " + task.getName());
    }
    System.out.println();
  }

}
