package com.camunda.demo.versicherungsneuantrag;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareAssertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.Variables.SerializationDataFormats;
import org.camunda.bpm.scenario.ProcessScenario;
import org.camunda.bpm.scenario.Scenario;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.camunda.demo.versicherungsneuantrag.adapter.SendEmailService;
import com.camunda.demo.versicherungsneuantrag.model.Neuantrag;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = { "com.camunda.demo.versicherungsneuantrag.adapter.*" })
@Deployment(resources = { "Versicherungsneuantrag.bpmn", "Dokumentenanforderung.bpmn", "Risikopruefung.dmn" })
public class VersicherungsneuantragScenarioTest {

  @Rule
  public ProcessEngineRule rule = new ProcessEngineRule();

  // Mock all waitstates in main process and call activity with a scenario
  @Mock
  private ProcessScenario insuranceApplication;
  @Mock
  private ProcessScenario documentRequest;
  @Mock
  private SendEmailService sendEmailService;
  
 
  @Before
  public void setup() throws Exception {
    MockitoAnnotations.initMocks(this);
    PowerMockito.whenNew(SendEmailService.class).withAnyArguments().thenReturn(sendEmailService);
  }    

  @Test
  public void testKeinRisikoDunkelverabreitung() throws Exception {
    Neuantrag neuantrag = DemoData.createNeuantrag(30, "VW", "Golf V");
    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    Scenario scenario = Scenario.run(insuranceApplication) //
        .startByKey(ProcessConstants.PROCESS_KEY_versicherungsneuantrag, variables).execute();

    assertThat(scenario.instance(insuranceApplication)).variables().containsEntry(ProcessConstants.VAR_NAME_risikobewertung, "grün");
    verify(insuranceApplication, never()).hasStarted("CallActivityNeuantragspruefung");
    verify(insuranceApplication).hasFinished("EndEventAntragPoliciert");
    
    verify(sendEmailService, times(1)).sendEmail(eq(neuantrag.getAntragssteller().getEmail()), anyString(), anyString());

  }

  @Test
  public void testRotesRisikoDunkelverabreitung() throws Exception {
    Neuantrag neuantrag = DemoData.createNeuantrag(20, "Porsche", "911");
    VariableMap variables = Variables.createVariables().putValue( //
        ProcessConstants.VAR_NAME_neuantrag, //
        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());

    Scenario scenario = Scenario.run(insuranceApplication) //
        .startByKey(ProcessConstants.PROCESS_KEY_versicherungsneuantrag, variables).execute();

    assertThat(scenario.instance(insuranceApplication)).variables().containsEntry(ProcessConstants.VAR_NAME_risikobewertung, "rot");
    verify(insuranceApplication, never()).hasStarted("CallActivityNeuantragspruefung");
    verify(insuranceApplication).hasFinished("EndEventAntragAbgelehnt");
    
    verify(sendEmailService, times(1)).sendEmail(eq(neuantrag.getAntragssteller().getEmail()), anyString(), anyString());

  }
  
//  @Test
//  public void testGelbesRisikoAkzeptiert() {
//    Neuantrag neuantrag = DemoData.createNeuantrag(30, true, "Porsche", "911");
//    VariableMap variables = Variables.createVariables().putValue( //
//        ProcessConstants.VAR_NAME_neuantrag, //
//        Variables.objectValue(neuantrag).serializationDataFormat(SerializationDataFormats.JSON).create());
//
//    when(insuranceApplication.waitsAtUserTask("UserTaskDecideAboutApplication")).thenReturn(new UserTaskAction() {      
//      @Override
//      public void execute(TaskDelegate task) throws Exception {
//        task.complete(withVariables("approved", true));
//      }
//    });
//    // Could be written more elegant with Java >= 8:
//    // when(insuranceApplication.waitsAtUserTask("UserTaskDecideAboutApplication")).thenReturn( (task) -> {
//    //    task.complete(withVariables("approved", true));
//    // });
//
//    when(insuranceApplication.runsCallActivity("CallActivityDocumentRequest")).thenReturn(Scenario.use(documentRequest));
//
//    Scenario scenario = Scenario.run(insuranceApplication).startByKey(ProcessConstants.PROCESS_KEY_versicherungsneuantrag, variables).execute();
//
//    assertThat(scenario.instance(insuranceApplication)).variables().containsEntry("riskAssessment", "yellow");
//    verify(insuranceApplication).hasCompleted("CallActivityNeuantragspruefung");
//    verify(insuranceApplication).hasFinished("EndEventAntragPoliciert");
//
//  }
//
//  @Test
//  public void testRedScenario() {
//
//    variables = Variables.createVariables().putValue("applicantAge", 20).putValue("carManufacturer", "Porsche").putValue("carType", "911");
//
//    Scenario scenario = Scenario.run(insuranceApplication).startByKey("InsuranceApplication", variables).execute();
//
//    assertThat(scenario.instance(insuranceApplication)).variables().containsEntry("riskAssessment", "red");
//
//    verify(insuranceApplication, never()).hasStarted("SubProcessManualCheck");
//    verify(insuranceApplication).hasFinished("EndEventApplicationRejected");
//
//  }
//
//  @Test
//  public void testManualApprovalScenario() {
//
//    variables = Variables.createVariables().putValue("applicantAge", 30).putValue("carManufacturer", "Porsche").putValue("carType", "911");
//
//    Scenario scenario = Scenario.run(insuranceApplication).startByKey("InsuranceApplication", variables).execute();
//
//    assertThat(scenario.instance(insuranceApplication)).variables().containsEntry("riskAssessment", "yellow").containsEntry("approved", true);
//
//    verify(insuranceApplication).hasCompleted("SubProcessManualCheck");
//    verify(insuranceApplication).hasFinished("EndEventApplicationAccepted");
//
//  }
//
//  @Test
//  public void testManualRejectionScenario() {
//
//    variables = Variables.createVariables().putValue("applicantAge", 30).putValue("carManufacturer", "Porsche").putValue("carType", "911");
//
//    when(insuranceApplication.waitsAtUserTask("UserTaskDecideAboutApplication")).thenReturn((task) -> {
//      task.complete(withVariables("approved", false));
//    });
//
//    Scenario scenario = Scenario.run(insuranceApplication).startByKey("InsuranceApplication", variables).execute();
//
//    assertThat(scenario.instance(insuranceApplication)).variables().containsEntry("riskAssessment", "yellow").containsEntry("approved", false);
//
//    verify(insuranceApplication).hasCompleted("SubProcessManualCheck");
//    verify(insuranceApplication).hasFinished("EndEventApplicationRejected");
//
//  }
//
//  @Test
//  public void testDocumentRequestScenario() {
//
//    variables = Variables.createVariables().putValue("applicantAge", 30).putValue("carManufacturer", "Porsche").putValue("carType", "911");
//
//    when(insuranceApplication.waitsAtUserTask("UserTaskDecideAboutApplication")).thenReturn((task) -> {
//      runtimeService().correlateMessage("msgDocumentNecessary");
//      task.complete(withVariables("approved", true));
//    });
//
//    Scenario.run(insuranceApplication).startByKey("InsuranceApplication", variables).execute();
//
//    verify(insuranceApplication).hasCompleted("CallActivityDocumentRequest");
//
//  }
//
//  @Test
//  public void testDocumentRequestBitLateScenario() {
//
//    variables = Variables.createVariables().putValue("applicantAge", 30).putValue("carManufacturer", "Porsche").putValue("carType", "911");
//
//    when(insuranceApplication.waitsAtUserTask("UserTaskDecideAboutApplication")).thenReturn((task) -> {
//      runtimeService().correlateMessage("msgDocumentNecessary");
//      task.complete(withVariables("approved", true));
//    });
//
//    when(documentRequest.waitsAtReceiveTask("ReceiveTaskWaitForDocuments")).thenReturn((receiveTask) -> {
//      receiveTask.defer("P1DT1M", receiveTask::receive);
//    });
//
//    Scenario.run(insuranceApplication).startByKey("InsuranceApplication", variables).execute();
//
//    verify(insuranceApplication).hasCompleted("CallActivityDocumentRequest");
//    verify(insuranceApplication, never()).hasStarted("UserTaskSpeedUpManualCheck");
//    verify(documentRequest).hasCompleted("SendTaskSendReminder");
//
//    verify(documentRequest, times(1)).waitsAtReceiveTask("ReceiveTaskWaitForDocuments");
//
//  }
//
//  @Test
//  public void testDocumentRequestVeryLateScenario() {
//
//    variables = Variables.createVariables().putValue("applicantAge", 30).putValue("carManufacturer", "Porsche").putValue("carType", "911");
//
//    when(insuranceApplication.waitsAtUserTask("UserTaskDecideAboutApplication")).thenReturn((task) -> {
//      runtimeService().correlateMessage("msgDocumentNecessary");
//      task.complete(withVariables("approved", true));
//    });
//
//    when(documentRequest.waitsAtReceiveTask("ReceiveTaskWaitForDocuments")).thenReturn((receiveTask) -> {
//      receiveTask.defer("P7DT1M", receiveTask::receive);
//    });
//
//    Scenario.run(insuranceApplication).startByKey("InsuranceApplication", variables).execute();
//
//    verify(insuranceApplication, times(1)).hasStarted("UserTaskSpeedUpManualCheck");
//    verify(insuranceApplication).hasCompleted("EndEventApplicationAccepted");
//
//    verify(documentRequest, times(1)).hasCompleted("UserTaskCallCustomer");
//    verify(documentRequest, times(5)).hasCompleted("SendTaskSendReminder");
//    verify(documentRequest).hasCanceled("ReceiveTaskWaitForDocuments");
//    verify(documentRequest, never()).hasCompleted("ReceiveTaskWaitForDocuments");
//
//  }

}
