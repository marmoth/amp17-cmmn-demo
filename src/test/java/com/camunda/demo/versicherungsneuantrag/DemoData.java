package com.camunda.demo.versicherungsneuantrag;

import java.util.Calendar;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.camunda.demo.versicherungsneuantrag.model.Neuantrag;
import com.camunda.demo.versicherungsneuantrag.model.Person;

public class DemoData {
  
  public static void main(String[] args) throws JsonProcessingException {
    System.out.println(new ObjectMapper().writeValueAsString(createNeuantrag(40, "BMW", "318i")));    
  }
  
  public static Neuantrag createNeuantrag(int alter, String hersteller, String typ) {
    Neuantrag neuantrag = new Neuantrag();
    neuantrag.setAntragssteller(new Person());

    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.YEAR, -1 * alter);

    neuantrag.getAntragssteller().setGeburtsdatum(cal.getTime());
    neuantrag.getAntragssteller().setName("Bernd Rücker");
    neuantrag.getAntragssteller().setEmail("trashcan@camunda.org");
    neuantrag.getAntragssteller().setGeschlecht("Mann");
    neuantrag.setFahrzeugHersteller(hersteller);
    neuantrag.setFahrzeugTyp(typ);
    neuantrag.setVersicherungsprodukt("Camundanzia Vollkasko Plus");
    return neuantrag;
  }
}
