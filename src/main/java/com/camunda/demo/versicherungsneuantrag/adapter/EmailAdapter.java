package com.camunda.demo.versicherungsneuantrag.adapter;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import com.camunda.demo.versicherungsneuantrag.ProcessConstants;
import com.camunda.demo.versicherungsneuantrag.model.Neuantrag;

public class EmailAdapter implements JavaDelegate {

  @Named("emailAdapter")
  @Override
  public void execute(DelegateExecution execution) throws Exception {
    // get service reference
    SendEmailService sendEmailService = new SendEmailService();

    // data input mapping
    String mailtext = (String) execution.getVariable("mailBody");
    String subject = (String) execution.getVariable("mailSubject");
    Neuantrag antrag = (Neuantrag) execution.getVariable(ProcessConstants.VAR_NAME_neuantrag);
    String email = antrag.getAntragssteller().getEmail();

    // service call
    sendEmailService.sendEmail(email, mailtext, subject);
  }
}
