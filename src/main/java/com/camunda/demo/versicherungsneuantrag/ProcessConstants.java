package com.camunda.demo.versicherungsneuantrag;

public class ProcessConstants {

  public static final String PROCESS_KEY_versicherungsneuantrag = "versicherungsneuantrag";
  public static final String CASE_KEY_Neuantragspruefung = "neuantragspruefung";

  public static final String PROCESS_KEY_dokumentenanforderung = "dokumentAnfordern";
  public static final String DECISION_KEY_risikopruefung = "risikopruefung";
  
  public static final String VAR_NAME_neuantrag = "neuantrag";
  public static final String VAR_NAME_risks = "risks";
  public static final String VAR_NAME_statusRedRisks = "statusRedRisks";
  public static final String VAR_NAME_approved = "approved";
  public static final String VAR_NAME_hinweise = "hinweise";
  public static final String VAR_NAME_refernceId = "referenceId";
  public static final String VAR_NAME_documentXml = "documentXml";
  public static final String VAR_NAME_document = "document";
  public static final String VAR_NAME_risikobewertung = "risikobewertung";

}
