package com.camunda.demo.versicherungsneuantrag;


import static com.camunda.demo.versicherungsneuantrag.ProcessConstants.CASE_KEY_Neuantragspruefung;
import static com.camunda.demo.versicherungsneuantrag.ProcessConstants.DECISION_KEY_risikopruefung;
import static com.camunda.demo.versicherungsneuantrag.ProcessConstants.PROCESS_KEY_dokumentenanforderung;
import static com.camunda.demo.versicherungsneuantrag.ProcessConstants.PROCESS_KEY_versicherungsneuantrag;

import org.camunda.bpm.application.PostDeploy;
import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.ProcessApplicationReference;
import org.camunda.bpm.application.impl.ServletProcessApplication;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.authorization.Permission;
import org.camunda.bpm.engine.authorization.Permissions;
import org.camunda.bpm.engine.authorization.Resources;

import com.camunda.consulting.util.LicenseHelper;
import com.camunda.consulting.util.UserGenerator;
import com.camunda.demo.environment.DemoDataGenerator;

import static com.camunda.consulting.util.UserGenerator.*;
import static com.camunda.consulting.util.FilterGenerator.*;

@ProcessApplication
public class VersicherungsneuantragProcessApplication extends ServletProcessApplication {

  @PostDeploy
  public void setupEnvironmentForDemo(ProcessEngine engine) {
    LicenseHelper.setLicense(engine);
    UserGenerator.createDefaultUsers(engine);
    setupUsersForDemo(engine);
  }
  
  public static void generateDemoData(ProcessEngine engine, ProcessApplicationReference reference) {
//    DemoDataGenerator.autoGenerateFor(engine, PROCESS_KEY_versicherungsneuantrag, reference, CASE_KEY_Neuantragspruefung, PROCESS_KEY_dokumentenanforderung, DECISION_KEY_risikopruefung);    
  }
  
  public static void setupUsersForDemo(ProcessEngine engine) {    
    addUser(engine, "marc", "marc", "Marc", "Mustermann");
    addGroup(engine, "sachbearbeiter", "Sachbearbeiter", "marc");
    addFilterGroupAuthorization(engine, "sachbearbeiter", FILTER_MeineAufgaben, FILTER_GruppenAufgaben, FILTER_Ueberfaellig, FILTER_Wiedervorlage);

    addUser(engine, "hugo", "hugo", "Hugo", "Halbmann");
    addGroup(engine, "gruppenleiter", "Gruppenleiter", "hugo");
    addFilterGroupAuthorization(engine, "gruppenleiter", FILTER_MeineAufgaben, FILTER_GruppenAufgaben, FILTER_Ueberfaellig, FILTER_Wiedervorlage);

    addUser(engine, "susi", "susi", "Susi", "Sonnenschein");
    addGroup(engine, "underwriter", "Underwriter", "susi");
    addFilterGroupAuthorization(engine, "underwriter", FILTER_MeineAufgaben, FILTER_GruppenAufgaben, FILTER_Ueberfaellig, FILTER_Wiedervorlage);

    addUser(engine, "paul", "paul", "Paul", "Pohl");
    addGroup(engine, "management", "Management", "paul");
    addFilterUserAuthorization(engine, "paul", FILTER_MeineAufgaben, FILTER_GruppenAufgaben, FILTER_Ueberfaellig, FILTER_Wiedervorlage, FILTER_PostkorbManagement, FILTER_alleAufgaben);
    
    createGrantGroupAuthorization(engine, 
        new String[]{"sachbearbeiter", "underwriter", "gruppenleiter"},
        new Permission[]{Permissions.READ, Permissions.READ_HISTORY, Permissions.UPDATE_INSTANCE},
        Resources.PROCESS_DEFINITION,
        new String[] {"versicherungsneuantragMitDokumentenerstellung", "versicherungsneuantrag"});
    
    // Admin Paul
    createGrantUserAuthorization(engine, 
        new String[]{"paul"},
        new Permission[]{Permissions.READ, Permissions.READ_HISTORY, Permissions.READ_INSTANCE, Permissions.UPDATE_INSTANCE},
        Resources.PROCESS_DEFINITION,
        new String[] {"versicherungsneuantrag", "dokumentAnfordern"});
    createGrantUserAuthorization(engine, 
        new String[]{"paul"},
        new Permission[]{Permissions.READ, Permissions.READ_HISTORY},
        Resources.DECISION_DEFINITION,
        new String[] {"risikopruefung"});
    createGrantUserAuthorization(engine, 
        new String[]{"paul"},
        new Permission[]{Permissions.READ, Permissions.UPDATE},
        Resources.TASK,
        new String[] {"*"});    
    createGrantUserAuthorization(engine, 
        new String[]{"paul"},
        new Permission[]{Permissions.ALL},
        Resources.DEPLOYMENT,
        new String[] {"*"});    
    createGrantUserAuthorization(engine, 
        new String[]{"paul"},
        new Permission[]{Permissions.ACCESS},
        Resources.APPLICATION,
        new String[] {"cockpit"});    
  }

}
